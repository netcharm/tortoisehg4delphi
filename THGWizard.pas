unit THgWizard;

{$R *.res}

interface

uses
    ToolsAPI, SysUtils, Windows, Dialogs, Menus, Registry, ShellApi,
    Classes, Controls, Graphics, ImgList, ExtCtrls, ActnList;

const
    Hg_PROJECT_EXPLORER   =  0;
    Hg_SN0                =  1; // '-'
    Hg_COMMIT             =  2;
    Hg_STATUS             =  3;
    Hg_VDIFF              =  4;
    Hg_SN1                =  5; // '-'
    Hg_ADD                =  6;
    Hg_REVERT             =  7;
    Hg_RENAME             =  8;
    Hg_FORGET             =  9;
    Hg_REMOVE             = 10;
    Hg_SN2                = 11; // '-'
    Hg_LOG                = 12;
    Hg_UPDATE             = 13;
    Hg_GREP               = 14;
    Hg_SN3                = 15; // '-'
    Hg_SYNCH              = 16;
    Hg_SERVE              = 17;
    Hg_CLONE              = 18;
    Hg_INIT               = 19;
    Hg_ARCHIVE            = 20;
    Hg_SN4                = 21; // '-'
    Hg_HGIGNORE           = 22;
    Hg_GUESS              = 23;
    Hg_SN5                = 24; // '-'
    Hg_REPOCONF           = 25;
    Hg_USERCONF           = 26;
    Hg_SN6                = 27; // '-'
    Hg_ABOUT              = 28;
    Hg_VERB_COUNT         = 29;


type
  TTortoiseHg = class(TNotifierObject, IOTANotifier, IOTAWizard)
  private
      timer: TTimer;
      tHgMenu: TMenuItem;
      THgPath: string;
      procedure Tick( Sender: TObject );
      procedure THgMenuClick( Sender: TObject );
      procedure DiffClick( Sender: TObject );
      procedure THgExec( params: string );
      function GetRepoPath(APath: string):string;
      function GetBitmapName(Index: Integer): string;
      function GetVerb(Index: Integer): string;
      function GetVerbState(Index: Integer): Word;
      procedure ExecuteVerb(Index: Integer);
      procedure CreateMenu;
      procedure UpdateAction( Sender: TObject );
      procedure ExecuteAction( Sender: TObject );
  public
      constructor Create;
      destructor Destroy; override;
      function GetIDString: string;
      function GetName: string;
      function GetState: TWizardState;
      procedure Execute;
  end;


{$IFNDEF DLL_MODE}

procedure Register;

{$ELSE}

function InitWizard( const BorlandIDEServices: IBorlandIDEServices;
                     RegisterProc: TWizardRegisterProc;
                     var Terminate: TWizardTerminateProc): Boolean; stdcall;

{$ENDIF}


implementation

uses
  //jvgnugettext;
  gnugettext;

function GetCurrentProject: IOTAProject;
var
  ModServices: IOTAModuleServices;
  Module: IOTAModule;
  Project: IOTAProject;
  ProjectGroup: IOTAProjectGroup;
  i: Integer;
begin
  Result := nil;
  ModServices := BorlandIDEServices as IOTAModuleServices;
  if ModServices <> nil then
  begin
    for i := 0 to ModServices.ModuleCount - 1 do
    begin
      Module := ModServices.Modules[i];
      if Supports(Module, IOTAProjectGroup, ProjectGroup) then
        begin
          Result := ProjectGroup.ActiveProject;
          Exit;
        end
      else if Supports(Module, IOTAProject, Project) then
        begin // In the case of unbound packages, return the 1st
          if Result = nil then
            Result := Project;
        end;
    end;
  end;
end;

procedure GetCurrentModuleFileList( fileList: TStrings );
var
  ModServices: IOTAModuleServices;
  Module: IOTAModule;
  i: integer;
begin
  if assigned(fileList) then
  begin
    fileList.Clear;
    ModServices := BorlandIDEServices as IOTAModuleServices;
    if ModServices <> nil then
    begin
      Module:= ModServices.CurrentModule;
      if Module <> nil then
        for i:= 0 to ModServices.CurrentModule.GetModuleFileCount-1 do
          fileList.Add( ModServices.CurrentModule.GetModuleFileEditor(i).GetFileName );
    end;
  end;
end;

function GetAppModuleName():string;
var
  fName: PChar;
begin
  // Get Dll Path
  fName := StrAlloc(MAX_PATH);
  //GetModuleFileName(GetModuleHandle('dll������'), fName, MAX_PATH);
  GetModuleFilename(HInstance, fName, MAX_PATH);
  Result := ChangeFileExt(ExtractFileName(fName), '');
  StrDispose(fName);
end;

function GetAppModulePath():string;
var
  fName: PChar;
begin
  // Get Dll Path
  fName := StrAlloc(MAX_PATH);
  //GetModuleFileName(GetModuleHandle('dll������'), fName, MAX_PATH);
  GetModuleFilename(HInstance, fName, MAX_PATH);
  Result := ExtractFileDir(fName);
  StrDispose(fName);
end;

constructor TTortoiseHg.Create;
var
  reg: TRegistry;
  DomainName: string;
  DomainPath: string;
begin
  Reg := TRegistry.Create;
  try
    Reg.RootKey := HKEY_LOCAL_MACHINE;
    if Reg.OpenKeyReadOnly( '\SOFTWARE\TortoiseHg' ) then
      THgPath := Reg.ReadString( '' )+'\thgw.exe';
  finally
    Reg.CloseKey;
    Reg.Free;
  end;

  tHgMenu := nil;

  timer := TTimer.create(nil);
  timer.interval := 200;
  timer.OnTimer  := tick;
  timer.enabled  := true;

  // localized UI Language
  DomainName := GetAppModuleName();
  DomainPath := GetAppModulePath()+'\locale';

  TextDomain(DomainName);
  AddDomainForResourceString(DomainName);
  BindTextDomain(DomainName, DomainPath);
end;

procedure TTortoiseHg.Tick( Sender: TObject );
var
  intf: INTAServices;
begin
  if BorlandIDEServices.QueryInterface( INTAServices, intf ) = s_OK then
  begin
    Self.CreateMenu;
    timer.Free;
    timer := nil;
  end;
end;

procedure TTortoiseHg.THgMenuClick( Sender: TObject );
var
  files: TStringList;
  i: integer;
  diff, item: TMenuItem;
begin
  // update the diff item and submenu; the diff action is handled by the
  // menu item itself, not by the action list
  diff := tHgMenu.Items[Hg_VDIFF];
  diff.Action  := nil;
  diff.OnClick := nil;
  diff.Enabled := false;
  diff.Clear;
  files := TStringList.Create;
  GetCurrentModuleFileList(files);
  if files.Count > 0 then
  begin
    diff.Enabled := true;
    diff.Caption := _('Diff');
    if files.Count > 1 then
      begin
        for i := 0 to files.Count-1 do
        begin
          item := TMenuItem.Create(diff);
          item.Caption := ExtractFileName( files[i] );
          item.OnClick := DiffClick;
          item.Tag     := i;
          diff.Add( item );
        end;
      end
    else
      begin  // files.Count = 1
        diff.Caption := _('Diff ') + ExtractFileName( files[0] );
        diff.OnClick := DiffClick;
      end;
  end;
  files.Free;
end;

procedure TTortoiseHg.DiffClick( Sender: TObject );
var
  files: TStringList;
  item: TComponent;
  project: IOTAProject;
  projpath: string;
  repopath: string;
begin
  project:= GetCurrentProject();
  projpath := ExcludeTrailingBackslash(ExtractFilePath(project.GetFileName));
  repopath := GetRepoPath(projpath);

  item:= Sender as TComponent;
  files:= TStringList.Create;
  GetCurrentModuleFileList(files);
  if files.Count > 1 then
    THgExec( 'vdiff ' + repopath + ' ' + AnsiQuotedStr( files[item.Tag], '"' ) )
  else if files.Count = 1 then
    THgExec( 'vdiff ' + repopath + ' ' + AnsiQuotedStr( files[0], '"' ) );
  files.Free;
end;

procedure TTortoiseHg.CreateMenu;
var
  mainMenu: TMainMenu;
  item: TMenuItem;
  i: integer;
  bmp: TBitmap;
  action: TAction;
begin
  if tHgMenu <> nil then exit;

  tHgMenu := TMenuItem.Create(nil);
  tHgMenu.Caption := 'TortoiseHg';
  tHgMenu.OnClick := THgMenuClick;

  for i := 0 to Hg_VERB_COUNT-1 do
  begin

    bmp:= TBitmap.Create;
    try
      bmp.LoadFromResourceName( HInstance, getBitmapName(i) );
    except

    end;

    action:= TAction.Create(nil);
    action.ActionList := (BorlandIDEServices as INTAServices).ActionList;
    action.Caption    := getVerb(i);
    action.Hint       := getVerb(i);
    if (bmp.Width = 16) and (bmp.height = 16) then
      action.ImageIndex := (BorlandIDEServices as INTAServices).AddMasked( bmp, clBlack );
    bmp.Free;
    action.OnUpdate   := updateAction;
    action.OnExecute  := executeAction;
    action.Tag        := i;

    item := TMenuItem.Create( tHgMenu );
    item.action := action;

    tHgMenu.Add( item );
  end;

  mainMenu := (BorlandIDEServices as INTAServices).MainMenu;
  mainMenu.Items.Insert( mainMenu.Items.Count-1, tHgMenu );

end;

destructor TTortoiseHg.Destroy;
begin
  if tHgMenu <> nil then
  begin
    tHgMenu.Free;
  end;
  inherited;
end;

function TTortoiseHg.GetBitmapName(Index: Integer): string;
begin
  case index of
    Hg_PROJECT_EXPLORER : Result := 'explorer';
    Hg_LOG              : Result := 'log';
    Hg_UPDATE           : Result := 'update';
    Hg_COMMIT           : Result := 'commit';
    Hg_REVERT           : Result := 'revert';
    Hg_CLONE            : Result := 'clone';
    Hg_INIT             : Result := 'createrepos';
    Hg_ADD              : Result := 'fileadd';
    Hg_FORGET           : Result := 'delete';
    Hg_REMOVE           : Result := 'filedelete';
    Hg_RENAME           : Result := 'rename';
    Hg_GUESS            : Result := 'guess';
    Hg_VDIFF            : Result := 'diff';
    Hg_STATUS           : Result := 'showchanged';
    Hg_GREP             : Result := 'grep';
    Hg_SYNCH            : Result := 'synch';
    Hg_SERVE            : Result := 'serve';
    Hg_ARCHIVE          : Result := 'archive';
    Hg_HGIGNORE         : Result := 'ignore';
    Hg_REPOCONF         : Result := 'repoconf';
    Hg_USERCONF         : Result := 'settings';
    Hg_ABOUT            : Result := 'about';
  end;
end;

function TTortoiseHg.GetVerb(Index: Integer): string;
begin
  case index of
    Hg_SN0              : Result := '-';
    Hg_SN1              : Result := '-';
    Hg_SN2              : Result := '-';
    Hg_SN3              : Result := '-';
    Hg_SN4              : Result := '-';
    Hg_SN5              : Result := '-';
    Hg_SN6              : Result := '-';

    Hg_PROJECT_EXPLORER : Result := _('Project explorer');
    Hg_LOG              : Result := _('Workbench');
    Hg_UPDATE           : Result := _('Update...');
    Hg_COMMIT           : Result := _('Commit...');
    Hg_REVERT           : Result := _('Revert Files...');
    Hg_CLONE            : Result := _('Clone...');
    Hg_INIT             : Result := _('Create Repository Here');
    Hg_ADD              : Result := _('Add Files...');
    Hg_FORGET           : Result := _('Forget Files...');
    Hg_REMOVE           : Result := _('Remove Files...');
    Hg_RENAME           : Result := _('Rename File');
    Hg_GUESS            : Result := _('Guess Renames');
    Hg_VDIFF            : Result := _('Visual Diff');
    Hg_STATUS           : Result := _('File Status');
    Hg_GREP             : Result := _('Search History');
    Hg_SYNCH            : Result := _('Synchronize');
    Hg_ARCHIVE          : Result := _('Archive');
    Hg_SERVE            : Result := _('Web Server');
    Hg_HGIGNORE         : Result := _('Edit Ignore Filter');
    Hg_REPOCONF         : Result := _('Repository Settings');
    Hg_USERCONF         : Result := _('Global Settings');
    Hg_ABOUT            : Result := _('About TortoiseHg');
  end;
end;

const vsEnabled = 1;

function TTortoiseHg.GetVerbState(Index: Integer): Word;
begin
  Result:= 0;
  case index of
    Hg_PROJECT_EXPLORER : if GetCurrentProject <> nil then Result := vsEnabled;
    //Hg_LOG              : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_LOG              :                                  Result := vsEnabled;
    Hg_UPDATE           : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_COMMIT           : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_REVERT           : if GetCurrentProject <> nil then Result := vsEnabled;
    //Hg_CLONE            : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_CLONE            :                                  Result := vsEnabled;
    //Hg_INIT             : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_INIT             :                                  Result := vsEnabled;
    Hg_ADD              : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_FORGET           : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_REMOVE           : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_RENAME           : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_GUESS            : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_VDIFF            : ;
    Hg_STATUS           : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_GREP             : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_SYNCH            : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_ARCHIVE          : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_SERVE            : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_HGIGNORE         : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_REPOCONF         : if GetCurrentProject <> nil then Result := vsEnabled;
    Hg_USERCONF         :                                  Result := vsEnabled;
    Hg_ABOUT            :                                  Result := vsEnabled;
  end;
end;

procedure TTortoiseHg.THgExec( params: string );
begin
  //MessageDlg( pchar( THgPath + ' ' + params ), mtConfirmation, [mbYes, mbNo], 0 );
  ShellExecute( 0, 'open', PChar(THgPath), PChar( params ), '.\', SW_SHOWNORMAL );
end;

function TTortoiseHg.GetRepoPath(APath: string): string;
var
  Repo: string;
  Found: Boolean;
begin
  Result := '';
  Found := False;

  if Length(APath)>0 then
  begin
    //Repo := ExcludeTrailingBackslash(ExpandFileName(APath));
    Repo := ExcludeTrailingBackslash(APath);

    while Length(Repo)>=3 do
    begin
      //if MessageDlg( pchar( Repo ), mtConfirmation, [mbYes, mbNo], 0 ) = mrNo then break;
      if DirectoryExists(Repo+'\'+'.hg') then
      begin
        Found := True;
        break;
      end;
      if SameText(Repo, ExtractFileDir(Repo)) then break;
      Repo := ExtractFileDir(Repo);
    end;
    if Found then Result := ' -R '+ AnsiQuotedStr( repo, '"');
  end;
end;

procedure TTortoiseHg.ExecuteVerb(Index: Integer);
var
  files: TStringList;

  project: IOTAProject;
  projpath: string;
  repopath: string;
  filename: string;
begin
  project  := GetCurrentProject();
  if assigned(project) then
    begin
      projpath := ExcludeTrailingBackslash(ExtractFilePath(project.GetFileName));
      repopath := GetRepoPath(projpath);
    end
  else
    begin
      projpath := GetCurrentDir();
      repopath := ' ' + projpath;
    end;
  //MessageDlg( pchar( projpath ), mtConfirmation, [mbYes, mbNo], 0 );

  files:= TStringList.Create;
  GetCurrentModuleFileList(files);
  if files.Count>0 then
    filename := ' ' + AnsiQuotedStr( files[0], '"' )
  else
    filename := '';
  files.Free;

  case index of
    Hg_PROJECT_EXPLORER :
      if project <> nil then
        ShellExecute( 0, 'open', PChar(projpath), '', '', SW_SHOWNORMAL );
    Hg_LOG              :
      //if project <> nil then
        THgExec( 'log' + repopath );
    Hg_UPDATE           :
      if project <> nil then
      begin
        if MessageDlg( _('All project files will be saved before update. Continue?'), mtConfirmation, [mbYes, mbNo], 0 ) = mrYes then
        begin
          (BorlandIDEServices as IOTAModuleServices).SaveAll;
          THgExec( 'update' + repopath );
        end;
      end;
    Hg_COMMIT           :
      if project <> nil then
      begin
        if MessageDlg( _('All project files will be saved before commit. Continue?'), mtConfirmation, [mbYes, mbNo], 0 ) = mrYes then
        begin
          (BorlandIDEServices as IOTAModuleServices).SaveAll;
          THgExec( 'commit' + repopath );
        end;
      end;
    Hg_REVERT           :
      if project <> nil then
        THgExec( 'revert' + repopath );
    Hg_CLONE            :
      //if project <> nil then
        THgExec( 'clone' + repopath );
    Hg_INIT             :
      //if project <> nil then
        THgExec( 'init ' + projpath );
    Hg_ADD              :
      if project <> nil then
        THgExec( 'add' + repopath );
    Hg_FORGET           :
      if project <> nil then
        THgExec( 'forget' + repopath );
    Hg_REMOVE           :
      if project <> nil then
        THgExec( 'remove' + repopath );
    Hg_RENAME           :
      if project <> nil then
        THgExec( 'rename' + repopath + filename );
    Hg_GUESS            :
      if project <> nil then
        THgExec( 'guess' + repopath );
    Hg_VDIFF            : ;
//      if project <> nil then
//        THgExec( 'vdiff' + repopath );
    Hg_STATUS           :
      if project <> nil then
        THgExec( 'status' + repopath );
    Hg_GREP             :
      if project <> nil then
        THgExec( 'grep' + repopath + filename );
    Hg_SYNCH            :
      if project <> nil then
        THgExec( 'synch' + repopath );
    Hg_SERVE            :
      if project <> nil then
        THgExec( 'serve' + repopath );
    Hg_ARCHIVE            :
      if project <> nil then
        THgExec( 'archive' + repopath );
    Hg_HGIGNORE         :
      if project <> nil then
        THgExec( 'hgignore' + repopath );
    Hg_REPOCONF         :
      if project <> nil then
        THgExec( 'repoconfig' + repopath );
    Hg_USERCONF         :
      //if project <> nil then
        THgExec( 'userconf' );
    Hg_ABOUT            :
      //if project <> nil then
        THgExec( 'about' );
  end;
end;

procedure TTortoiseHg.UpdateAction( Sender: TObject );
var
  action: TAction;
begin
  action:= Sender as TAction;
  action.Enabled := getVerbState( action.tag ) = vsEnabled;
end;

procedure TTortoiseHg.ExecuteAction( Sender: TObject );
var
  action: TAction;
begin
  action := Sender as TAction;
  ExecuteVerb( action.tag );
end;

function TTortoiseHg.GetIDString: string;
begin
  Result := 'Mercurial.TortoiseHg';
end;

function TTortoiseHg.GetName: string;
begin
  Result := 'TortoiseHg Add-In';
end;

function TTortoiseHg.GetState: TWizardState;
begin
  Result := [wsEnabled];
end;

procedure TTortoiseHg.Execute;
begin
//
end;


{$IFNDEF DLL_MODE}

procedure Register;
begin
  RegisterPackageWizard(TTortoiseHg.create);
end;

{$ELSE}

var wizardID: integer;

procedure FinalizeWizard;
var
  WizardServices: IOTAWizardServices;
begin
  Assert(Assigned(BorlandIDEServices));

  WizardServices := BorlandIDEServices as IOTAWizardServices;
  Assert(Assigned(WizardServices));

  WizardServices.RemoveWizard( wizardID );
end;

function InitWizard( const BorlandIDEServices: IBorlandIDEServices;
                     RegisterProc: TWizardRegisterProc;
                     var Terminate: TWizardTerminateProc): Boolean; stdcall;
var
  WizardServices: IOTAWizardServices;
begin
  Assert(BorlandIDEServices <> nil);
  Assert(ToolsAPI.BorlandIDEServices = BorlandIDEServices);

  Terminate := FinalizeWizard;

  WizardServices := BorlandIDEServices as IOTAWizardServices;
  Assert(Assigned(WizardServices));

  wizardID := WizardServices.AddWizard(TTortoiseHg.Create as IOTAWizard);

  Result := wizardID >= 0;
end;


exports
  InitWizard name WizardEntryPoint;

{$ENDIF}



end.

